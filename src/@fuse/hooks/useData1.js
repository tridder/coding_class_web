import {useCallback, useState} from 'react';
import _ from '@lodash';

function useData1(initialState, onSubmit)
{
    const [data1, setData1] = useState(initialState);

    const handleChangeData1 = useCallback((event) => {
        event.persist();
        setData1(
            data1 => ({
                ...data1,
                [event.target.name]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
            })
        );
    }, []);

    const resetData1 = useCallback(() => {
        setData1(initialState);
    }, [initialState]);

    const setInData1 = useCallback((name, value) => {
        setData1(data1 => _.setIn(data1, name, value));
    }, []);

    const handleSubmitData1 = useCallback((event) => {
        if ( event )
        {
            event.preventDefault();
        }
        if ( onSubmit )
        {
            onSubmit();
        }
    }, [onSubmit]);

    return {
        data1,
        handleChangeData1,
        handleSubmitData1,
        resetData1,
        setData1,
        setInData1
    }
}

export default useData1;
