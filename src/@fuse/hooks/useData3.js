import {useCallback, useState} from 'react';
import _ from '@lodash';

function useData3(initialState, onSubmit)
{
    const [data3, setData3] = useState(initialState);

    const handleChangeData3 = useCallback((event) => {
        event.persist();
        setData3(
            data3 => ({
                ...data3,
                [event.target.name]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
            })
        );
    }, []);

    const resetData3 = useCallback(() => {
        setData3(initialState);
    }, [initialState]);

    const setInData3 = useCallback((name, value) => {
        setData3(data3 => _.setIn(data3, name, value));
    }, []);

    const handleSubmitData3 = useCallback((event) => {
        if ( event )
        {
            event.preventDefault();
        }
        if ( onSubmit )
        {
            onSubmit();
        }
    }, [onSubmit]);

    return {
        data3,
        handleChangeData3,
        handleSubmitData3,
        resetData3,
        setData3,
        setInData3
    }
}

export default useData3;
