import {useCallback, useState} from 'react';
import _ from '@lodash';

function useData2(initialState, onSubmit)
{
    const [data2, setData2] = useState(initialState);

    const handleChangeData2 = useCallback((event) => {
        event.persist();
        setData2(
            data2 => ({
                ...data2,
                [event.target.name]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
            })
        );
    }, []);

    const resetData2 = useCallback(() => {
        setData2(initialState);
    }, [initialState]);

    const setInData2 = useCallback((name, value) => {
        setData2(data2 => _.setIn(data2, name, value));
    }, []);

    const handleSubmitData2 = useCallback((event) => {
        if ( event )
        {
            event.preventDefault();
        }
        if ( onSubmit )
        {
            onSubmit();
        }
    }, [onSubmit]);

    return {
        data2,
        handleChangeData2,
        handleSubmitData2,
        resetData2,
        setData2,
        setInData2
    }
}

export default useData2;
