import {useCallback, useState} from 'react';
import _ from '@lodash';

function useData4(initialState, onSubmit)
{
    const [data4, setData4] = useState(initialState);

    const handleChangeData4 = useCallback((event) => {
        event.persist();
        setData4(
            data4 => ({
                ...data4,
                [event.target.name]: event.target.type === 'checkbox' ? event.target.checked : event.target.value
            })
        );
    }, []);

    const resetData4 = useCallback(() => {
        setData4(initialState);
    }, [initialState]);

    const setInData4 = useCallback((name, value) => {
        setData4(data4 => _.setIn(data4, name, value));
    }, []);

    const handleSubmitData4 = useCallback((event) => {
        if ( event )
        {
            event.preventDefault();
        }
        if ( onSubmit )
        {
            onSubmit();
        }
    }, [onSubmit]);

    return {
        data4,
        handleChangeData4,
        handleSubmitData4,
        resetData4,
        setData4,
        setInData4
    }
}

export default useData4;
