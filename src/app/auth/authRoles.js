/**
 * Authorization Roles
 */
const authRoles = {
    coordinator    : ['coordinator'],
    teacher    : ['coordinator', 'teacher'],
    student    : ['coordinator', 'student'],
    user     : ['coordinator', 'student', 'teacher'],
    onlyGuest: []
};


export default authRoles;
