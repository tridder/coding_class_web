import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse';
import {HomeConfig} from 'app/main/home/HomeConfig';
import {CareersConfig} from 'app/main/apps/careers/CareersConfig';
import {FacultiesConfig} from 'app/main/apps/faculties/FacultiesConfig';
import {TeachersConfig} from 'app/main/apps/teachers/TeachersConfig';
import {StudentsConfig} from 'app/main/apps/students/StudentsConfig';
import {GroupsConfig} from 'app/main/apps/groups/GroupsConfig';
import {LoginConfig} from 'app/main/login/LoginConfig';
import {LogoutConfig} from 'app/main/logout/LogoutConfig';
import {authRoles} from 'app/auth';
import _ from 'lodash';

function setUser(configs,user)
{
    if(user === "coordinator"){
        return configs.map(config => _.merge({}, config, {auth: authRoles.coordinator}))
    }
    else if(user === "teacher"){
        return configs.map(config => _.merge({}, config, {auth: authRoles.teacher}))
    }
    else if(user === "student"){
        return configs.map(config => _.merge({}, config, {auth: authRoles.student}))
    }
    else if(user === "user"){
        return configs.map(config => _.merge({}, config, {auth: authRoles.user}))
    }
}

const routeConfigs = [
    ...setUser([
        LogoutConfig,
        HomeConfig
    ],"user"),
    ...setUser([
        FacultiesConfig,
        CareersConfig,
        TeachersConfig,
        StudentsConfig,
        GroupsConfig
    ],"coordinator"),
    LoginConfig
];

const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs),
    {
        path     : '/',
        component: () => <Redirect to="/home"/>,
    },
    {
        component: () => <Redirect to="/error-500"/>
    }
];

export default routes;
