import {authRoles} from 'app/auth';
const navigationConfig = [
    {
        'id'   : 'home',
        'title': 'Home',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/home'
    },
    {
        'id'   : 'faculty',
        'title': 'Faculty',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/apps/faculties',
        auth   : authRoles.coordinator,
    },
    {
        'id'   : 'career',
        'title': 'Career',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/apps/careers',
        auth   : authRoles.coordinator,
    },
    {
        'id'   : 'teacher',
        'title': 'Teacher',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/apps/teachers',
        auth   : authRoles.coordinator,
    },
    {
        'id'   : 'student',
        'title': 'Student',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/apps/students',
        auth   : authRoles.coordinator,
    },
    {
        'id'   : 'group',
        'title': 'Group',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/apps/groups',
        auth   : authRoles.coordinator,
    },
    {
        'id'      : 'auth',
        'title'   : 'Session',
        'type'    : 'collapse',
        'icon'    : 'apps',
        'children': [
            {
                'id'   : 'login',
                'title': 'Login',
                'type' : 'item',
                'url'  : '/login',
                auth   : authRoles.onlyGuest,
                'icon' : 'lock'
            },
            {
                'id'   : 'logout',
                'title': 'Logout',
                'type' : 'item',
                auth   : authRoles.user,
                'url'  : '/logout',
                'icon' : 'exit_to_app'
            }
        ]
    }
];

export default navigationConfig;
