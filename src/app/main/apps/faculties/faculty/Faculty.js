import React, {useEffect} from 'react';
import {Button, TextField, Icon, Typography, Grid, Divider, AppBar, Tab, Tabs, FormControlLabel, Switch, MenuItem} from '@material-ui/core/index';
import {FuseAnimate, FusePageCarded} from '@fuse';
import {useForm} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import _ from '@lodash';
import * as Actions2 from 'app/store/actions';

function Faculty(props)
{
    const dispatch = useDispatch();
    const faculty = useSelector(({FacultiesApp}) => FacultiesApp.faculty);
    const {form, handleChange, setForm, setInForm} = useForm(null);


    useEffect(() => {
        function updateFacultyState()
        {
            const params = props.match.params;
            const {facultyId} = params;

            if ( facultyId === 'new' )
            {
                dispatch(Actions.newFaculty());
            }
            else
            {
                dispatch(Actions.getFaculty(props.match.params));
            }
        }

        updateFacultyState();
    }, [dispatch, props.match.params]);

    useEffect(() => {

        if ((faculty.data.faculty && !form) || (faculty.data.faculty && form && faculty.data.faculty.id !== form.id))
        {
            setForm(faculty.data.faculty);
        }
    }, [form, faculty.data, setForm]);

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography id="users" className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/faculties/faculties" color={"inherit"}>
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    Faculties
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'New Faculty'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Faculty Detail</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap"
                                variant="contained"
                                onClick={() => dispatch(Actions.saveFaculty(form,props.match.params.facultyId === "new" ? 'insert' : "update",props.history))}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-16 sm:p-24" >
                        <div>
                            <Grid
                                container
                                spacing={2}
                                direction="row"

                            >
                                <Grid container spacing={3}>
                                    <Grid item xs={12} lg={12}>
                                        <TextField
                                            label="Name"
                                            id="name"
                                            name="name"
                                            value={form.name}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                </Grid>

                            </Grid>
                        </div>
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('FacultiesApp', reducer)(Faculty);
