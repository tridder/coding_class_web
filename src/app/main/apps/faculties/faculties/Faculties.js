import React from 'react';
import {FusePageCarded} from '@fuse';
import withReducer from 'app/store/withReducer';
import FacultiesTable from './FacultiesTable';
import FacultiesHeader from './FacultiesHeader';
import reducer from '../store/reducers';

function Faculties()
{
    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <FacultiesHeader/>
            }
            content={
                <FacultiesTable/>
            }
            innerScroll
        />
    );
}

export default withReducer('FacultiesApp', reducer)(Faculties);
