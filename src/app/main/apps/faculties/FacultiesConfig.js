import React from 'react';
import Faculty from './faculty/Faculty'
import Faculties from './faculties/Faculties'
import {Redirect} from 'react-router-dom';

export const FacultiesConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/faculties/faculties/:facultyId',
            component: Faculty
        },
        {
            path     : '/apps/faculties/faculties',
            component: Faculties
        },
        {
            path     : '/apps/faculties',
            component: () => <Redirect to="/apps/faculties/faculties"/>
        }
    ]
};
