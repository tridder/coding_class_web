import axios from 'axios';
import global from '../../../../../../global'

export const GET_FACULTIES = '[_FACULTIES APP] GET _FACULTIES';
export const SET__FACULTIES_SEARCH_TEXT = '[_FACULTIES APP] SET _FACULTIES SEARCH TEXT';

export function getFaculties()
{
    const request = axios.post(global.ip+global.port+'/api/faculty/select');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_FACULTIES,
                payload: response.data.faculties
            })
        );
}

export function setFacultiesSearchText(event)
{
    return {
        type      : SET__FACULTIES_SEARCH_TEXT,
        searchText: event.target.value
    }
}

