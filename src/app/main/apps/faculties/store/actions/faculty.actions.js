import axios from 'axios';
import global from '../../../../../../global'
import React from "react";
import {showMessage} from 'app/store/actions/fuse';
import {Grid} from '@material-ui/core';

export const GET_FACULTY = '[FACULTY APP] GET FACULTY';
export const SAVE_FACULTY= '[FACULTY APP] SAVE FACULTY';

export function getFaculty(params)
{
    var data = {
        id : params.facultyId
    };
    const request = axios.post(global.ip+global.port+'/api/faculty/search', data,null);
    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_FACULTY,
                payload: response.data.data
            })
        );

}

export function saveFaculty(data,param,history)
{
    let valid = [];
    if(data.name === null  || data.name === "") valid.push('Name')

    if(valid.length === 0){
        const request = axios.post(global.ip+global.port+'/api/faculty/'+param, data);
        return (dispatch) =>
            request.then((response) => {
                console.log(response.data)
                if(response.data.code === "OK"){
                    dispatch(showMessage({message: 'Faculty Saved'}));
                    history.push('/apps/faculties')
                }else{
                    dispatch(showMessage({
                        message: 'Name repeated',
                        autoHideDuration: 1500,
                        anchorOrigin: {
                            vertical  : 'top',
                            horizontal: 'center'
                        },
                        variant: 'info'
                    }));
                }
                }
            );
    }else{
        return (dispatch) =>
            dispatch(showMessage({
                message : <div>
                    <Grid container spacing={1}>
                        <Grid item xs={12}><strong>Missing Data</strong></Grid>
                        {valid.map((value,index) => {
                            return <Grid key={index} item xs={4}>{value}</Grid>
                        })}
                    </Grid></div>,
                autoHideDuration: 1500,//ms
                anchorOrigin: {
                    vertical  : 'top',//top bottom
                    horizontal: 'center'//left center right
                },
                variant: 'info'//success error info warning null
            }));
    }
}


export function newFaculty()
{
    const faculty = {
        faculty: {
            name: ''
        }
    };

    let data = {}
    let data2 = {}
    return (dispatch) => {
        return axios.post(global.ip+global.port+'/api/faculty/new-faculty')
            .then(response => {
                data = response.data.data;
                data2 = {...faculty, ...data};
                dispatch({
                    type   : GET_FACULTY,
                    payload: data2
                })
            })
            .catch(error => {
                throw(error);
            });
    };
}
