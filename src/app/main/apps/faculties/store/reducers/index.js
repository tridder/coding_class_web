import {combineReducers} from 'redux';
import faculties from './faculties.reducer';
import faculty from './faculty.reducer';

const reducer = combineReducers({
    faculties,
    faculty
});

export default reducer;
