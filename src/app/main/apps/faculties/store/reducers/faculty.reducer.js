import * as Actions from '../actions';

const initialState = {
    data: {}
};

const faculty = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_FACULTY:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_FACULTY:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default faculty;
