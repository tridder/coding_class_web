import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const facultiesReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_FACULTIES:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET__FACULTIES_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default facultiesReducer;
