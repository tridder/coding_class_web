import * as Actions from '../actions';

const initialState = {
    data: {}
};

const student = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_STUDENT:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_STUDENT:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default student;
