import {combineReducers} from 'redux';
import students from './students.reducer';
import student from './student.reducer';

const reducer = combineReducers({
    students,
    student
});

export default reducer;
