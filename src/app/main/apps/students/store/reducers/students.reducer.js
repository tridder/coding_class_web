import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const studentsReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_STUDENTS:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_STUDENTS_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default studentsReducer;
