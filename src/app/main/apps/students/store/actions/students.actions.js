import axios from 'axios';
import global from '../../../../../../global'

export const GET_STUDENTS = '[STUDENTS APP] GET STUDENTS';
export const SET_STUDENTS_SEARCH_TEXT = '[STUDENTS APP] SET STUDENTS SEARCH TEXT';

export function getStudents()
{
    const request = axios.post(global.ip+global.port+'/api/student/select');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_STUDENTS,
                payload: response.data.students
            })
        );
}

export function setStudentsSearchText(event)
{
    return {
        type      : SET_STUDENTS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

