import axios from 'axios';
import global from '../../../../../../global'
import React from "react";
import {showMessage} from 'app/store/actions/fuse';
import {Grid} from '@material-ui/core';

export const GET_STUDENT = '[STUDENT APP] GET STUDENT';
export const SAVE_STUDENT = '[STUDENT APP] SAVE STUDENT';

export function getStudent(params)
{
    var data = {
        id : params.studentId
    };
    const request = axios.post(global.ip+global.port+'/api/student/search', data,null);
    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_STUDENT,
                payload: response.data.data
            })
        );

}

export function saveStudent(data,param,history)
{
    let valid = [];
    if(data.username === null  || data.username === "") valid.push('Username')
    if(data.password === null  || data.password === "") valid.push('Password')
    if(data.name === null  || data.name === "") valid.push('Name')
    if(data.lastName === null  || data.lastName === "") valid.push('Last Name')
    if(data.birthDate === null  || data.birthDate === "") valid.push('Birth Date')
    // if(data.photo === null  || data.photo === "") valid.push('Photo')
    // if(data.hireDate === null  || data.hireDate === "") valid.push('Hire Date')

    if(valid.length === 0){
        const request = axios.post(global.ip+global.port+'/api/student/'+param, data);
        return (dispatch) =>
            request.then((response) => {
                if(response.data.code === "OK"){
                    dispatch(showMessage({message: 'Student Saved'}));
                    history.push('/apps/students')
                }else{
                    dispatch(showMessage({
                        message: 'Username repeated',
                        autoHideDuration: 1500,
                        anchorOrigin: {
                            vertical  : 'top',
                            horizontal: 'center'
                        },
                        variant: 'info'
                    }));
                }
                }
            );
    }else{
        return (dispatch) =>
            dispatch(showMessage({
                message : <div>
                    <Grid container spacing={1}>
                        <Grid item xs={12}><strong>Missing Data</strong></Grid>
                        {valid.map((value,index) => {
                            return <Grid key={index} item xs={4}>{value}</Grid>
                        })}
                    </Grid></div>,
                autoHideDuration: 1500,//ms
                anchorOrigin: {
                    vertical  : 'top',//top bottom
                    horizontal: 'center'//left center right
                },
                variant: 'info'//success error info warning null
            }));
    }
}


export function newStudent()
{
    const student = {
        student: {
            username: '',
            password: '',
            status: 1,
            name: '',
            lastName: '',
            birthDate: Date.now(),
            levelId: {id: 3},
            photo: ''
        }
    };

    let data = {}
    let data2 = {}
    return (dispatch) => {
        return axios.post(global.ip+global.port+'/api/student/new-student')
            .then(response => {
                data = response.data.data;
                data2 = {...student, ...data};
                dispatch({
                    type   : GET_STUDENT,
                    payload: data2
                })
            })
            .catch(error => {
                throw(error);
            });
    };
}
