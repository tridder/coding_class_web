import React from 'react';
import Student from './student/Student'
import Students from './students/Students'
import {Redirect} from 'react-router-dom';

export const StudentsConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/students/students/:studentId',
            component: Student
        },
        {
            path     : '/apps/students/students',
            component: Students
        },
        {
            path     : '/apps/students',
            component: () => <Redirect to="/apps/students/students"/>
        }
    ]
};
