import React from 'react';
import {FusePageCarded} from '@fuse';
import withReducer from 'app/store/withReducer';
import StudentsTable from './StudentsTable';
import StudentsHeader from './StudentsHeader';
import reducer from '../store/reducers';

function Students()
{
    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <StudentsHeader/>
            }
            content={
                <StudentsTable/>
            }
            innerScroll
        />
    );
}

export default withReducer('StudentsApp', reducer)(Students);
