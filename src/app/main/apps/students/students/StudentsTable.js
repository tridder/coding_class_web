import React, {useEffect, useState} from 'react';
import {Table, TableBody, TableCell, TablePagination, TableRow} from '@material-ui/core';
import {FuseScrollbars} from '@fuse';
import {withRouter} from 'react-router-dom';
import _ from '@lodash';
import StudentsTableHead from './StudentsTableHead';
import * as Actions from '../store/actions';
import {useDispatch, useSelector} from 'react-redux';

function StudentsTable(props)
{
    const dispatch = useDispatch();
    const students = useSelector(({StudentsApp}) => StudentsApp.students.data);
    const searchText = useSelector(({StudentsApp}) => StudentsApp.students.searchText);

    const [selected, setSelected] = useState([]);
    const [data, setData] = useState(students);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [order, setOrder] = useState({
        direction: 'asc',
        id       : null
    });

    useEffect(() => {
        dispatch(Actions.getStudents());
    }, [dispatch]);

    useEffect(() => {
        setData(searchText.length === 0 ? students : _.filter(students, item => item.name.toLowerCase().includes(searchText.toLowerCase())))
    }, [students, searchText]);

    function handleRequestSort(event, property)
    {
        const id = property;
        let direction = 'desc';

        if ( order.id === property && order.direction === 'desc' )
        {
            direction = 'asc';
        }

        setOrder({
            direction,
            id
        });
    }

    function handleSelectAllClick(event)
    {
        if ( event.target.checked )
        {
            setSelected(data.map(n => n.id));
            return;
        }
        setSelected([]);
    }

    function handleClick(item)
    {
        props.history.push('/apps/students/students/' + item.id);
    }

    function handleChangePage(event, page)
    {
        setPage(page);
    }

    function handleChangeRowsPerPage(event)
    {
        setRowsPerPage(event.target.value);
    }

    return (
        <div className="w-full flex flex-col">

            <FuseScrollbars className="flex-grow overflow-x-auto">

                <Table className="min-w-xl" aria-labelledby="tableTitle">

                    <StudentsTableHead
                        numSelected={selected.length}
                        order={order}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={data.length}
                    />

                    <TableBody>
                        {_.orderBy(data, [
                            (o) => {
                                switch ( order.id )
                                {
                                    case 'categories':
                                    {
                                        return o.categories[0];
                                    }
                                    default:
                                    {
                                        return o[order.id];
                                    }
                                }
                            }
                        ], [order.direction])
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map(n => {
                                const isSelected = selected.indexOf(n.id) !== -1;
                                return (
                                    <TableRow
                                        className="h-64 cursor-pointer"
                                        hover
                                        role="checkbox"
                                        aria-checked={isSelected}
                                        tabIndex={-1}
                                        key={n.id}
                                        selected={isSelected}
                                        onClick={event => handleClick(n)}
                                    >
                                        <TableCell  component="th" scope="row" padding="none" align="center">
                                            {n.photo != null && n.photo != "" ? (
                                                <img className="w-36 rounded" src={n.photo} alt={n.name}/>
                                            ) : (
                                                <img className="w-36 rounded" src="assets/images/users/defaultUser.png" alt={n.name}/>
                                            )}
                                        </TableCell>

                                        <TableCell component="th" scope="row" align="center">
                                            {n.name}
                                        </TableCell>

                                        <TableCell component="th" scope="row" align="center">
                                            {n.lastName}
                                        </TableCell>

                                        <TableCell component="th" scope="row" align="center">
                                            {n.username}
                                        </TableCell>

                                        <TableCell component="th" scope="row" align="center">
                                            {n.status ? "Activo" : "Inactivo"}
                                        </TableCell>

                                    </TableRow>
                                );
                            })}
                    </TableBody>
                </Table>
            </FuseScrollbars>

            <TablePagination
                component="div"
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                    'aria-label': 'Previous Page'
                }}
                nextIconButtonProps={{
                    'aria-label': 'Next Page'
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    );
}

export default withRouter(StudentsTable);
