import React from 'react';
import {TableHead, TableSortLabel, TableCell, TableRow, Tooltip} from '@material-ui/core';

const rows = [
    { id: 'image', align: 'center', disablePadding: true, label: 'Image', sort:false},
    { id: 'name', align: 'center',disablePadding: false, label: 'Name', sort:true },
    { id: 'lastName',align: 'center',disablePadding: false, label: 'Last Name', sort:true  },
    { id: 'username',align: 'center',disablePadding: false, label: 'Username', sort:true  },
    { id: 'status', align: 'center',disablePadding: false, label: 'Status', sort:true  }
];


function StudentsTableHead(props)
{

    const createSortHandler = property => event => {
        props.onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow className="h-64">
                {rows.map(row => {
                    return (
                        <TableCell
                            key={row.id}
                            align={row.align}
                            padding={row.disablePadding ? 'none' : 'default'}
                            sortDirection={props.order.id === row.id ? props.order.direction : false}
                        >
                            {row.sort && (
                                <Tooltip
                                    title="Sort"
                                    placement={row.align === "right" ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={props.order.id === row.id}
                                        direction={props.order.direction}
                                        onClick={createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            )}
                        </TableCell>
                    );
                }, this)}
            </TableRow>
        </TableHead>
    );
}

export default StudentsTableHead;
