import React, {useEffect} from 'react';
import {Button, TextField, Icon, Typography, Grid, Divider, AppBar, Tab, Tabs, FormControlLabel, Switch, MenuItem} from '@material-ui/core/index';
import {FuseAnimate, FusePageCarded} from '@fuse';
import {useForm, useData1, useData2} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import _ from '@lodash';
import moment from "moment"

function Student(props)
{
    const dispatch = useDispatch();
    const student = useSelector(({StudentsApp}) => StudentsApp.student);
    const {form, handleChange, setForm, setInForm} = useForm(null);


    useEffect(() => {
        function updateStudentState()
        {
            const params = props.match.params;
            const {studentId} = params;

            if ( studentId === 'new' )
            {
                dispatch(Actions.newStudent());
            }
            else
            {
                dispatch(Actions.getStudent(props.match.params));
            }
        }

        updateStudentState();
    }, [dispatch, props.match.params]);

    useEffect(() => {

        if ((student.data.student && !form) || (student.data.student && form && student.data.student.id !== form.id))
        {
            setForm(student.data.student);
        }
    }, [form, student.data, setForm]);

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography id="users" className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/students/students" color={"inherit"}>
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    Students
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'New Student'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Student Detail</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap"
                                variant="contained"
                                onClick={() => dispatch(Actions.saveStudent(form,props.match.params.studentId === "new" ? 'insert' : "update",props.history))}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-16 sm:p-24" >
                        <div>
                            <Grid
                                container
                                spacing={2}
                                direction="row"

                            >
                                <Grid container spacing={3}>
                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Name"
                                            id="name"
                                            name="name"
                                            value={form.name}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Last Name"
                                            id="lastName"
                                            name="lastName"
                                            value={form.lastName}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Username"
                                            id="username"
                                            name="username"
                                            value={form.username}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Password"
                                            id="password"
                                            name="password"
                                            value={form.password}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={12}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker
                                                variant="inline"
                                                inputVariant="outlined"
                                                label="Birth Date"
                                                name="birthDate"
                                                fullWidth
                                                margin="normal"
                                                value={moment(form.birthDate).format('MM/DD/YYYY')}
                                                onChange={(event) => setForm(_.set({...form}, 'birthDate', moment(event).format('YYYY-MM-DD')))}
                                                format="dd/MM/yyyy"
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>

                                </Grid>

                            </Grid>
                        </div>
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('StudentsApp', reducer)(Student);
