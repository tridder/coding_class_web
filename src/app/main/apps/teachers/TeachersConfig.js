import React from 'react';
import Teacher from './teacher/Teacher'
import Teachers from './teachers/Teachers'
import {Redirect} from 'react-router-dom';

export const TeachersConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/teachers/teachers/:teacherId',
            component: Teacher
        },
        {
            path     : '/apps/teachers/teachers',
            component: Teachers
        },
        {
            path     : '/apps/teachers',
            component: () => <Redirect to="/apps/teachers/teachers"/>
        }
    ]
};
