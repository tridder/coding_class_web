import axios from 'axios';
import global from '../../../../../../global'
import React from "react";
import {showMessage} from 'app/store/actions/fuse';
import {Grid} from '@material-ui/core';

export const GET_TEACHER = '[TEACHER APP] GET TEACHER';
export const SAVE_TEACHER = '[TEACHER APP] SAVE TEACHER';

export function getTeacher(params)
{
    var data = {
        id : params.teacherId
    };
    const request = axios.post(global.ip+global.port+'/api/teacher/search', data,null);
    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_TEACHER,
                payload: response.data.data
            })
        );

}

export function saveTeacher(data,param,history)
{
    let valid = [];
    if(data.username === null  || data.username === "") valid.push('Username')
    if(data.password === null  || data.password === "") valid.push('Password')
    if(data.name === null  || data.name === "") valid.push('Name')
    if(data.lastName === null  || data.lastName === "") valid.push('Last Name')
    if(data.birthDate === null  || data.birthDate === "") valid.push('Birth Date')
    // if(data.photo === null  || data.photo === "") valid.push('Photo')
    if(data.hireDate === null  || data.hireDate === "") valid.push('Hire Date')

    if(valid.length === 0){
        const request = axios.post(global.ip+global.port+'/api/teacher/'+param, data);
        return (dispatch) =>
            request.then((response) => {
                if(response.data.code === "OK"){
                    dispatch(showMessage({message: 'Teacher Saved'}));
                    history.push('/apps/teachers')
                }else{
                    dispatch(showMessage({
                        message: 'Username repeated',
                        autoHideDuration: 1500,
                        anchorOrigin: {
                            vertical  : 'top',
                            horizontal: 'center'
                        },
                        variant: 'info'
                    }));
                }
                }
            );
    }else{
        return (dispatch) =>
            dispatch(showMessage({
                message : <div>
                    <Grid container spacing={1}>
                        <Grid item xs={12}><strong>Missing Data</strong></Grid>
                        {valid.map((value,index) => {
                            return <Grid key={index} item xs={4}>{value}</Grid>
                        })}
                    </Grid></div>,
                autoHideDuration: 1500,//ms
                anchorOrigin: {
                    vertical  : 'top',//top bottom
                    horizontal: 'center'//left center right
                },
                variant: 'info'//success error info warning null
            }));
    }
}


export function newTeacher()
{
    const teacher = {
        teacher: {
            username: '',
            password: '',
            status: 1,
            name: '',
            lastName: '',
            birthDate: Date.now(),
            levelId: {id: 2},
            photo: '',
            hireDate: Date.now()
        }
    };

    let data = {}
    let data2 = {}
    return (dispatch) => {
        return axios.post(global.ip+global.port+'/api/teacher/new-teacher')
            .then(response => {
                data = response.data.data;
                data2 = {...teacher, ...data};
                dispatch({
                    type   : GET_TEACHER,
                    payload: data2
                })
            })
            .catch(error => {
                throw(error);
            });
    };
}
