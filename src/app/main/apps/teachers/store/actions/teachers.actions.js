import axios from 'axios';
import global from '../../../../../../global'

export const GET_TEACHERS = '[TEACHERS APP] GET TEACHERS';
export const SET_TEACHERS_SEARCH_TEXT = '[TEACHERS APP] SET TEACHERS SEARCH TEXT';

export function getTeachers()
{
    const request = axios.post(global.ip+global.port+'/api/teacher/select');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_TEACHERS,
                payload: response.data.teachers
            })
        );
}

export function setTeachersSearchText(event)
{
    return {
        type      : SET_TEACHERS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

