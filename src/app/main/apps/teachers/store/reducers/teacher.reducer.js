import * as Actions from '../actions';

const initialState = {
    data: {}
};

const teacher = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_TEACHER:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_TEACHER:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default teacher;
