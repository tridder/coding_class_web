import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const teachersReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_TEACHERS:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_TEACHERS_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default teachersReducer;
