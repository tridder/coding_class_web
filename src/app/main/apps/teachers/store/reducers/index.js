import {combineReducers} from 'redux';
import teachers from './teachers.reducer';
import teacher from './teacher.reducer';

const reducer = combineReducers({
    teachers,
    teacher
});

export default reducer;
