import {combineReducers} from 'redux';
import groups from './groups.reducer';
import group from './group.reducer';

const reducer = combineReducers({
    groups,
    group
});

export default reducer;
