import axios from 'axios';
import global from '../../../../../../global'

export const GET_GROUPS = '[GROUPS APP] GET GROUPS';
export const SET_GROUPS_SEARCH_TEXT = '[GROUPS APP] SET GROUPS SEARCH TEXT';

export function getGroups()
{
    const request = axios.post(global.ip+global.port+'/api/group/select');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_GROUPS,
                payload: response.data.groups
            })
        );
}

export function setGroupsSearchText(event)
{
    return {
        type      : SET_GROUPS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

