import axios from 'axios';
import global from '../../../../../../global'
import React from "react";
import {showMessage} from 'app/store/actions/fuse';
import {Grid} from '@material-ui/core';

export const GET_GROUP = '[GROUP APP] GET GROUP';
export const SAVE_GROUP = '[GROUP APP] SAVE GROUP';

export function getGroup(params)
{
    var data = {
        id : params.groupId
    };
    const request = axios.post(global.ip+global.port+'/api/group/search', data,null);
    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_GROUP,
                payload: response.data.data
            })
        );

}

export function saveGroup(data,param,history)
{
    let valid = [];
    if(data.name === null  || data.name === "") valid.push('Name')
    if(data.careerId.id === null  || data.careerId.id === "") valid.push('Career')
    if(data.startDate === null  || data.startDate === "") valid.push('Start Date')
    if(data.endDate === null  || data.endDate === "") valid.push('End Date')

    if(valid.length === 0){
        const request = axios.post(global.ip+global.port+'/api/group/'+param, data);
        return (dispatch) =>
            request.then((response) => {
                if(response.data.code === "OK"){
                    dispatch(showMessage({message: 'Group Saved'}));
                    history.push('/apps/groups')
                }else{
                    dispatch(showMessage({
                        message: 'Name repeated',
                        autoHideDuration: 1500,
                        anchorOrigin: {
                            vertical  : 'top',
                            horizontal: 'center'
                        },
                        variant: 'info'
                    }));
                }
                }
            );
    }else{
        return (dispatch) =>
            dispatch(showMessage({
                message : <div>
                    <Grid container spacing={1}>
                        <Grid item xs={12}><strong>Missing Data</strong></Grid>
                        {valid.map((value,index) => {
                            return <Grid key={index} item xs={4}>{value}</Grid>
                        })}
                    </Grid></div>,
                autoHideDuration: 1500,//ms
                anchorOrigin: {
                    vertical  : 'top',//top bottom
                    horizontal: 'center'//left center right
                },
                variant: 'info'//success error info warning null
            }));
    }
}


export function newGroup()
{
    const group = {
        group: {
            name: '',
            careerId: {id: ''},
            startDate: Date.now(),
            endDate: Date.now()
        }
    };

    let data = {}
    let data2 = {}
    return (dispatch) => {
        return axios.post(global.ip+global.port+'/api/group/new-group')
            .then(response => {
                data = response.data.data;
                data2 = {...group, ...data};
                dispatch({
                    type   : GET_GROUP,
                    payload: data2
                })
            })
            .catch(error => {
                throw(error);
            });
    };
}
