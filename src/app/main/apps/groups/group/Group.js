import React, {useEffect} from 'react';
import {Button, TextField, Icon, Typography, Grid, Divider, AppBar, Tab, Tabs, FormControlLabel, Switch, MenuItem} from '@material-ui/core/index';
import {FuseAnimate, FusePageCarded} from '@fuse';
import {useForm, useData1, useData2} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import _ from '@lodash';
import moment from "moment"

function Group(props)
{
    const dispatch = useDispatch();
    const group = useSelector(({GroupsApp}) => GroupsApp.group);
    const {form, handleChange, setForm, setInForm} = useForm(null);
    const {data1, handleChangeData1, setData1} = useData1(null);

    useEffect(() => {
        function updateGroupState()
        {
            const params = props.match.params;
            const {groupId} = params;

            if ( groupId === 'new' )
            {
                dispatch(Actions.newGroup());
            }
            else
            {
                dispatch(Actions.getGroup(props.match.params));
            }
        }

        updateGroupState();
    }, [dispatch, props.match.params]);

    useEffect(() => {

        if ((group.data.group && !form) || (group.data.group && form && group.data.group.id !== form.id))
        {
            setForm(group.data.group);
            setData1(group.data.careers);
        }
    }, [form, group.data, setForm, setData1]);

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography id="users" className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/groups/groups" color={"inherit"}>
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    Groups
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'New Group'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Group Detail</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap"
                                variant="contained"
                                onClick={() => dispatch(Actions.saveGroup(form,props.match.params.groupId === "new" ? 'insert' : "update",props.history))}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-16 sm:p-24" >
                        <div>
                            <Grid
                                container
                                spacing={2}
                                direction="row"

                            >
                                <Grid container spacing={3}>
                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Name"
                                            id="name"
                                            name="name"
                                            value={form.name}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            id="career"
                                            select
                                            label="Career"
                                            name="careerId.id"
                                            value={form.careerId.id}
                                            onChange={(event) => setInForm(event.target.name,event.target.value)}
                                            fullWidth
                                            margin="normal"
                                            variant="outlined"
                                            required
                                        >
                                            {data1.map(option => (
                                                <MenuItem key={option.id} value={option.id}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker
                                                variant="inline"
                                                inputVariant="outlined"
                                                label="Start Date"
                                                name="startDate"
                                                fullWidth
                                                margin="normal"
                                                value={moment(form.startDate).format('MM/DD/YYYY')}
                                                onChange={(event) => setForm(_.set({...form}, 'startDate', moment(event).format('YYYY-MM-DD')))}
                                                format="dd/MM/yyyy"
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                            <KeyboardDatePicker
                                                variant="inline"
                                                inputVariant="outlined"
                                                label="End Date"
                                                name="endDate"
                                                fullWidth
                                                margin="normal"
                                                value={moment(form.endDate).format('MM/DD/YYYY')}
                                                onChange={(event) => setForm(_.set({...form}, 'endDate', moment(event).format('YYYY-MM-DD')))}
                                                format="dd/MM/yyyy"
                                            />
                                        </MuiPickersUtilsProvider>
                                    </Grid>

                                </Grid>

                            </Grid>
                        </div>
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('GroupsApp', reducer)(Group);
