import React from 'react';
import Group from './group/Group'
import Groups from './groups/Groups'
import {Redirect} from 'react-router-dom';

export const GroupsConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/groups/groups/:groupId',
            component: Group
        },
        {
            path     : '/apps/groups/groups',
            component: Groups
        },
        {
            path     : '/apps/groups',
            component: () => <Redirect to="/apps/groups/groups"/>
        }
    ]
};
