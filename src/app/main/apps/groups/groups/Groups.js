import React from 'react';
import {FusePageCarded} from '@fuse';
import withReducer from 'app/store/withReducer';
import GroupsTable from './GroupsTable';
import GroupsHeader from './GroupsHeader';
import reducer from '../store/reducers';

function Groups()
{
    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <GroupsHeader/>
            }
            content={
                <GroupsTable/>
            }
            innerScroll
        />
    );
}

export default withReducer('GroupsApp', reducer)(Groups);
