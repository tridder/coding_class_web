import React, {useEffect} from 'react';
import {Button, TextField, Icon, Typography, Grid, Divider, AppBar, Tab, Tabs, FormControlLabel, Switch, MenuItem} from '@material-ui/core/index';
import {FuseAnimate, FusePageCarded} from '@fuse';
import {useForm, useData1, useData2} from '@fuse/hooks';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import withReducer from 'app/store/withReducer';
import * as Actions from '../store/actions';
import reducer from '../store/reducers';
import _ from '@lodash';
import * as Actions2 from 'app/store/actions';

function Career(props)
{
    const dispatch = useDispatch();
    const career = useSelector(({CareersApp}) => CareersApp.career);
    const {form, handleChange, setForm, setInForm} = useForm(null);
    const {data1, handleChangeData1, setData1} = useData1(null);
    const {data2, handleChangeData2, setData2} = useData2(null);


    useEffect(() => {
        function updateCareerState()
        {
            const params = props.match.params;
            const {careerId} = params;

            if ( careerId === 'new' )
            {
                dispatch(Actions.newCareer());
            }
            else
            {
                dispatch(Actions.getCareer(props.match.params));
            }
        }

        updateCareerState();
    }, [dispatch, props.match.params]);

    useEffect(() => {

        if ((career.data.career && !form) || (career.data.career && form && career.data.career.id !== form.id))
        {
            setForm(career.data.career);
            setData1(career.data.faculties);
            career.data.engineering.push({'id':0,'name': 'Without Engineering'});
            setData2(career.data.engineering);
        }
    }, [form, career.data, setForm, setData1, setData2]);

    return (
        <FusePageCarded
            classes={{
                toolbar: "p-0",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                form && (
                    <div className="flex flex-1 w-full items-center justify-between">

                        <div className="flex flex-col items-start max-w-full">

                            <FuseAnimate animation="transition.slideRightIn" delay={300}>
                                <Typography id="users" className="normal-case flex items-center sm:mb-12" component={Link} role="button" to="/apps/careers/careers" color={"inherit"}>
                                    <Icon className="mr-4 text-20">arrow_back</Icon>
                                    Careers
                                </Typography>
                            </FuseAnimate>

                            <div className="flex items-center max-w-full">
                                <div className="flex flex-col min-w-0">
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography className="text-16 sm:text-20 truncate">
                                            {form.name ? form.name : 'New Career'}
                                        </Typography>
                                    </FuseAnimate>
                                    <FuseAnimate animation="transition.slideLeftIn" delay={300}>
                                        <Typography variant="caption">Career Detail</Typography>
                                    </FuseAnimate>
                                </div>
                            </div>
                        </div>
                        <FuseAnimate animation="transition.slideRightIn" delay={300}>
                            <Button
                                className="whitespace-no-wrap"
                                variant="contained"
                                onClick={() => dispatch(Actions.saveCareer(form,props.match.params.careerId === "new" ? 'insert' : "update",props.history))}
                            >
                                Save
                            </Button>
                        </FuseAnimate>
                    </div>
                )
            }
            content={
                form && (
                    <div className="p-16 sm:p-24" >
                        <div>
                            <Grid
                                container
                                spacing={2}
                                direction="row"

                            >
                                <Grid container spacing={3}>
                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Name"
                                            id="name"
                                            name="name"
                                            value={form.name}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={6}>
                                        <TextField
                                            label="Area"
                                            id="area"
                                            name="area"
                                            value={form.area}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={5}>
                                        <TextField
                                            label="Total Quarters"
                                            id="totalQuarters"
                                            name="totalQuarters"
                                            value={form.totalQuarters}
                                            onChange={handleChange}
                                            variant="outlined"
                                            margin="normal"
                                            type="number"
                                            required
                                            style={{width: '100%'}}
                                        />
                                    </Grid>

                                    <Grid item xs={12} lg={5}>
                                        <TextField
                                            id="faculty"
                                            select
                                            label="Faculty"
                                            name="facultyId.id"
                                            value={form.facultyId.id}
                                            onChange={(event) => setInForm(event.target.name,event.target.value)}
                                            fullWidth
                                            margin="normal"
                                            variant="outlined"
                                            required
                                        >
                                            {data1.map(option => (
                                                <MenuItem key={option.id} value={option.id}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </Grid>

                                    <Grid item xs={12} lg={5}>
                                        <TextField
                                            id="engineering"
                                            select
                                            label="Engineering"
                                            name="ingId.id"
                                            value={form.ingId.id}
                                            onChange={(event) => setInForm(event.target.name,event.target.value)}
                                            fullWidth
                                            margin="normal"
                                            variant="outlined"
                                            required
                                        >
                                            {data2.map(option => (
                                                <MenuItem key={option.id} value={option.id}>
                                                    {option.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </Grid>

                                    <Grid item xs={12} lg={2} style={{display: 'flex',alignItems: 'center',justifyContent: 'center'}}>
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    checked={form.isTSU}
                                                    onChange={handleChange}
                                                    value={form.isTSU}
                                                    color="secondary"
                                                    name="isTSU"
                                                    margin="normal"
                                                />
                                            }
                                            label="Is TSU"
                                        />
                                    </Grid>

                                </Grid>

                            </Grid>
                        </div>
                    </div>
                )
            }
            innerScroll
        />
    )
}

export default withReducer('CareersApp', reducer)(Career);
