import React from 'react';
import {FusePageCarded} from '@fuse';
import withReducer from 'app/store/withReducer';
import CareersTable from './CareersTable';
import CareersHeader from './CareersHeader';
import reducer from '../store/reducers';

function Careers()
{
    return (
        <FusePageCarded
            classes={{
                content: "flex",
                header : "min-h-72 h-72 sm:h-136 sm:min-h-136"
            }}
            header={
                <CareersHeader/>
            }
            content={
                <CareersTable/>
            }
            innerScroll
        />
    );
}

export default withReducer('CareersApp', reducer)(Careers);
