import React from 'react';
import Career from './career/Career'
import Careers from './careers/Careers'
import {Redirect} from 'react-router-dom';

export const CareersConfig = {
    settings: {
        layout: {}
    },
    routes  : [
        {
            path     : '/apps/careers/careers/:careerId',
            component: Career
        },
        {
            path     : '/apps/careers/careers',
            component: Careers
        },
        {
            path     : '/apps/careers',
            component: () => <Redirect to="/apps/careers/careers"/>
        }
    ]
};
