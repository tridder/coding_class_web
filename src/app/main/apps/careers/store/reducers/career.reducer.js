import * as Actions from '../actions';

const initialState = {
    data: {}
};

const career = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_CAREER:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SAVE_CAREER:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        default:
        {
            return state;
        }
    }
};

export default career;
