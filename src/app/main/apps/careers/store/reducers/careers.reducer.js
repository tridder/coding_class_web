import * as Actions from '../actions';

const initialState = {
    data      : [],
    searchText: ''
};

const careersReducer = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.GET_CAREERS:
        {
            return {
                ...state,
                data: action.payload
            };
        }
        case Actions.SET_CAREERS_SEARCH_TEXT:
        {
            return {
                ...state,
                searchText: action.searchText
            };
        }
        default:
        {
            return state;
        }
    }
};

export default careersReducer;
