import {combineReducers} from 'redux';
import careers from './careers.reducer';
import career from './career.reducer';

const reducer = combineReducers({
    careers,
    career
});

export default reducer;
