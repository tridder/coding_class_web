import axios from 'axios';
import global from '../../../../../../global'

export const GET_CAREERS = '[CAREERS APP] GET CAREERS';
export const SET_CAREERS_SEARCH_TEXT = '[CAREERS APP] SET CAREERS SEARCH TEXT';

export function getCareers()
{
    const request = axios.post(global.ip+global.port+'/api/career/select');

    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_CAREERS,
                payload: response.data.careers
            })
        );
}

export function setCareersSearchText(event)
{
    return {
        type      : SET_CAREERS_SEARCH_TEXT,
        searchText: event.target.value
    }
}

