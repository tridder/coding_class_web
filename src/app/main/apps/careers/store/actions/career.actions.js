import axios from 'axios';
import global from '../../../../../../global'
import React from "react";
import {showMessage} from 'app/store/actions/fuse';
import {Grid} from '@material-ui/core';

export const GET_CAREER = '[CAREER APP] GET CAREER';
export const SAVE_CAREER= '[CAREER APP] SAVE CAREER';

export function getCareer(params)
{
    var data = {
        id : params.careerId
    };
    const request = axios.post(global.ip+global.port+'/api/career/search', data,null);
    return (dispatch) =>
        request.then((response) =>
            dispatch({
                type   : GET_CAREER,
                payload: response.data.data
            })
        );

}

export function saveCareer(data,param,history)
{
    let valid = [];
    if(data.name === null  || data.name === "") valid.push('Name')
    if(data.facultyId.id === null  || data.facultyId.id === "") valid.push('Faculty')
    if(data.totalQuarters === null  || data.totalQuarters === "") valid.push('Total Quarters')

    if(valid.length === 0){
        const request = axios.post(global.ip+global.port+'/api/career/'+param, data);
        return (dispatch) =>
            request.then((response) => {
                console.log(response.data)
                if(response.data.code === "OK"){
                    dispatch(showMessage({message: 'Career Saved'}));
                    history.push('/apps/careers')
                }else{
                    dispatch(showMessage({
                        message: 'Name and Area repeated',
                        autoHideDuration: 1500,
                        anchorOrigin: {
                            vertical  : 'top',
                            horizontal: 'center'
                        },
                        variant: 'info'
                    }));
                }
                }
            );
    }else{
        return (dispatch) =>
            dispatch(showMessage({
                message : <div>
                    <Grid container spacing={1}>
                        <Grid item xs={12}><strong>Missing Data</strong></Grid>
                        {valid.map((value,index) => {
                            return <Grid key={index} item xs={4}>{value}</Grid>
                        })}
                    </Grid></div>,
                autoHideDuration: 1500,//ms
                anchorOrigin: {
                    vertical  : 'top',//top bottom
                    horizontal: 'center'//left center right
                },
                variant: 'info'//success error info warning null
            }));
    }
}


export function newCareer()
{
    const career = {
        career: {
            name: '',
            area: '',
            facultyId: {id: ''},
            isTSU: false,
            totalQuarters: 9,
            ingId: {id: 0}
        }
    };

    let data = {}
    let data2 = {}
    return (dispatch) => {
        return axios.post(global.ip+global.port+'/api/career/new-career')
            .then(response => {
                data = response.data.data;
                data2 = {...career, ...data};
                dispatch({
                    type   : GET_CAREER,
                    payload: data2
                })
            })
            .catch(error => {
                throw(error);
            });
    };
}
